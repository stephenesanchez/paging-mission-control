package hii.alerts;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * The alerts class is used to store the alerts to be displayed to the user. 
 * @author stephen.sanchez
 *
 */
@Getter @Setter @AllArgsConstructor
public class Alert {
	
	private int satelliteId;
	private String severity;
	private String component;
	private String timeStamp;

}

