package hii.alerts;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.function.BiPredicate;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/** ---------------------------------------------------------------------------------------------------
 * This class stores the telemetry data that is read from a file.
 * @author stephen.sanchez
 *
------------------------------------------------------------------------------------------------------ */

@Getter @Setter @NoArgsConstructor
public class Telemetry {

	public static final int FIVE_MINUTES = 300000;
	private static final String pattern = "yyyy-MM-dd HH:mm:ss.SSS'Z'";
	


	private Date timeStamp;
    private int satelliteId;
    private float redHighLimit;
    private float yellowHighLimit;
    private float yelllowLowLimit;
    private float redLowLimit;
    private float rawValue ;
    private String component;

    
    public Telemetry(String timeStamp, int satelliteId, float redHighLimit, float yellowHighLimit,
			float yelllowLowLimit, float redLowLimit, float rawValue, String component) throws ParseException{

		this.satelliteId = satelliteId;
		this.redHighLimit = redHighLimit;
		this.yellowHighLimit = yellowHighLimit;
		this.yelllowLowLimit = yelllowLowLimit;
		this.redLowLimit = redLowLimit;
		this.rawValue = rawValue;
		this.component = component;
		
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
    	this.timeStamp = sdf.parse(timeStamp);

    }

    /**
     * 
     * @return the time stamp  formatted in the proper UTC Zulu format
     */
	public String getFormattedDate() {
		DateFormat df = new SimpleDateFormat(pattern);
		return df.format(timeStamp);
	}

	
	/**
	 * @param args - the telemtery file
	 * @throws FileNotFoundException
	 * @throws ParseException
	 */
	public static void main(String[] args) throws FileNotFoundException, ParseException {

		if (args.length != 1) {
			System.out.println("usage: Telemetry <filename>");
			System.exit(1);
		}

		File file = new File(args[0]);
		if (! file.exists()) 
			throw new FileNotFoundException("could not find the file " + args[0]);

		Logger logger = Logger.getLogger(Telemetry.class.getName());
		List<Telemetry> telemetry_array = new ArrayList<>();

		// read the file and create a new telemetry object from each line
		try (Scanner s = new Scanner(file)) {

			while (s.hasNextLine()) {
				String line = s.nextLine();
				String[] values = line.split("\\|");
				Telemetry t = new Telemetry(values[0], Integer.parseInt(values[1]), Float.parseFloat(values[2]),
						Float.parseFloat(values[3]), Float.parseFloat(values[4]), Float.parseFloat(values[5]),
						Float.parseFloat(values[6]), values[7]);
				telemetry_array.add(t);
				logger.fine( "time: {} " +  t.getFormattedDate()  + " satId: " + t.getSatelliteId()
					+ " redHigh:" + t.getRedHighLimit() + " rawValue:" + t.getRawValue() + " comp:" + t.getComponent());
			}
		}

		//Java Function that compares a telemetry thermostat object against an array of objects for thermostat high alert
		final BiPredicate<Telemetry, Telemetry> thermostatHigh = (t, a) ->
				   a.getComponent().equals("TSTAT") &&
				   t.getTimeStamp().getTime() <= a.getTimeStamp().getTime() &&
				   a.getRawValue() > a.getRedHighLimit() &&
				   t.getSatelliteId() == a.getSatelliteId() &&
				   t.getTimeStamp().getTime() <= (a.getTimeStamp().getTime() + Telemetry.FIVE_MINUTES);

		//Java Function that compares a telemetry battery object against an array of objects for battery low alert
		final BiPredicate<Telemetry, Telemetry> batteryLow = (t, a) -> 
		   a.getComponent().equals("BATT") &&
		   t.getTimeStamp().getTime() <= a.getTimeStamp().getTime() &&
		   a.getRawValue() < a.getRedLowLimit() &&
		   t.getSatelliteId() == a.getSatelliteId() &&
		   t.getTimeStamp().getTime() <= (a.getTimeStamp().getTime() + Telemetry.FIVE_MINUTES);

		
		List<Alert> alerts = new ArrayList<>();
		long thermostat_alerts = 0;
		long battery_alerts = 0;
		
		//loop through the telemetry data read from the file
		for (Telemetry t : telemetry_array) {
			//add  thermostat telemetry objects to alerts when the thermostat has three or more high values in rolling a 5 minute window
			if(t.component.equals("TSTAT")) {
				thermostat_alerts = telemetry_array.stream().filter(ta -> thermostatHigh.test(t, ta)).count();
				if (thermostat_alerts >= 3)
					alerts.add(new Alert(t.getSatelliteId(),"RED HIGH", t.getComponent(), t.getFormattedDate()));
				}
			//add  battery telemetry objects to alerts when the battery has three or more low values in rolling a 5 minute window
			else if (t.getComponent().equals("BATT")) {
				battery_alerts = telemetry_array.stream().filter(ta -> batteryLow.test(t, ta)).count();
				if (battery_alerts >= 3)
					alerts.add(new Alert(t.getSatelliteId(),"RED LOW", t.getComponent(), t.getFormattedDate()));
			}
			logger.fine("time = " + t.getFormattedDate() + " satId =" + t.getSatelliteId() + " component =" + t.getComponent() + "  count = " + thermostat_alerts );
		}

		//sort the list so it looks like the output required.  
		alerts.sort((Alert a , Alert b) -> a.getSeverity().compareTo(b.getSeverity()));
		
		//use Gson to write the list of alerts to the screen
		String json_string = new Gson().toJson(alerts);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonElement je = JsonParser.parseString(json_string);
		
		System.out.println(gson.toJson(je));
	}


}
