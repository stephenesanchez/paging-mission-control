#test_notifications.py
import json
from queue import Empty
import requests
import pytest

def test_get(): 
    url = 'http://127.0.0.1:8000/'
    resp = requests.get(url=url)
    print (resp.json())
    
    assert resp.status_code == 200

def test_alert():
    url = 'http://127.0.0.1:8000/alerts'
    test_file= {'file': open('data/inputfile.txt','rb')}
    resp = requests.post(url, files=test_file)
    assert resp.status_code == 200
  # Create Python object from JSON string data
    obj = json.loads(resp.content)
 
# Pretty Print JSON
    json_formatted_str = json.dumps(obj, indent=4)
    print(json_formatted_str)

