#Telemetry Class
""" class to hold the telemetry data from the satellites """
import datetime

from tokenize import String


class Telemetry:

    

    def __init__(self, timeStamp, satelliteId, redHighLimit, yellowHighLimit, yellowLowLimit, redLowLimit, rawValue, component): 
        self.timeStamp = timeStamp
        self.satelliteId = satelliteId
        self.redHighLimit = redHighLimit
        self.yellowHighLimit = yellowHighLimit
        self.yelllowLowLimit = yellowLowLimit
        self.redLowLimit = redLowLimit
        self.rawValue = rawValue
        self.component = component
        
    def toAlert(self, severity) : 
        
        dt_obj = datetime.datetime.strptime(self.timeStamp,'%Y-%m-%d %H:%M:%S.%f')
        dt_str = dt_obj.strftime("%Y-%m-%d %H:%M:%S.%f")[:-3] + 'Z'
        return { "satelliteId" : self.satelliteId,
                 "severity": severity,
                 "component": self.component,
                 "timestamp": dt_str
                }
        
    #     def __iter__(self):
    #         yield {
    #         "timeStamp": self.timeStamp,
    #         "satelliteId": self.satelliteId,
    #         "redHighLimit": self.redHighLimit,
    #         "yellowHighLimit": self.yellowHighLimit,
    #         "yellowLowLimit": self.yellowLowLimit,
    #         "redLowLimit": self.redLowLimit,
    #         "rawValue": self.rawValue,
    #         "component": self.component
    #     }

    # def __str__(self):
    #     return json.dumps(self, ensure_ascii=False, cls=CustomEncoder)

    # def __repr__(self):
    #     return self.__str__()
    