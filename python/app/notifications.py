from operator import truediv
from typing import List
import uvicorn
import json

from fastapi import File, UploadFile, FastAPI, HTTPException
from Telemetry import Telemetry
from datetime import timedelta, datetime

app = FastAPI()

def inTimeWindow(t : Telemetry, t2: Telemetry):
    t_time = datetime.strptime(t.timeStamp,'%Y-%m-%d %H:%M:%S.%f')
    t2_time = datetime.strptime(t2.timeStamp,'%Y-%m-%d %H:%M:%S.%f')

    if(t2_time <= t_time + timedelta(minutes=5)) : 
        return True
     
    return False

def hasTSTATAlert( t : Telemetry, tlist : List ):
    t2 : Telemetry
    """check the component battery for low value"""
    if (t.component == "TSTAT" and  t.rawValue > t.redHighLimit) : 
        count = 1        
        for t2  in tlist :
            if (t2.component == "TSTAT" and 
                t.satelliteId == t2.satelliteId and 
                t2.rawValue > t2.redHighLimit and 
                inTimeWindow(t,t2))  :              
                count +=1        
                if (count == 3) :  
                    return True
    
    return False        


def hasBatteryAlert( t : Telemetry, tlist : List ):
    t2 : Telemetry
    """check the component battery for low value"""
    if (t.component == "BATT" and  t.rawValue < t.redLowLimit) : 
        count = 1        
        for t2  in tlist :
            if (t2.component == "BATT" and 
                t.satelliteId == t2.satelliteId and 
                t2.rawValue < t2.redLowLimit and 
                inTimeWindow(t,t2))  :              
                count += 1
                if (count == 3) :  
                    return True
    
    return False        
            

def createTelemetry(line) : 
    """grab each value on the line"""
    t = [x.strip() for x in line.split('|') if x]
    dt_obj = datetime.strptime(t[0],'%Y%m%d %H:%M:%S.%f')
    
    """create the Telemetry object from the line of data."""
    telemetry = Telemetry(str(dt_obj),int(t[1]),float(t[2]) ,float(t[3]),float(t[4]),float(t[5]),float(t[6]),t[7])
    return telemetry
    
@app.get("/")
def ping():
    return {"Status": "Up!"}

@app.post("/alerts")
def alerts(file: UploadFile= File(...)): 
    try:
        telem_data = []
        """Read each line into a telemetry object"""
        for line in file.file.readlines() : 
             telem_data.append(createTelemetry(line.decode('utf-8')))
        
        tal = []
        bal = []
        
        """can probably look into using a lambda here, but this works for now"""       
        for i in range(len(telem_data)-1) :
            if (hasTSTATAlert(telem_data[i], telem_data[i+1:])) : 
                tal.append(telem_data[i].toAlert("RED HIGH"))
            if (hasBatteryAlert(telem_data[i], telem_data[i+1:])) : 
                 bal.append(telem_data[i].toAlert("RED LOW"))
                 
        """merge the alert lists with TSTAT first then BATT"""
        alerts_list = tal + bal
        json_string = json.dumps(alerts_list, indent=4)
    except Exception as e:
        raise HTTPException(status_code = 500, detail = e.args)
    finally:
        file.file.close()
    print (json_string)
    return json.loads(json_string)

if __name__ == "__main__" :
    uvicorn.run(app,host="0.0.0.0", port=8000)